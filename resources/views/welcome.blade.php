@extends('Layouts.Main')

@section('title', 'LifeTec')

@section('content')

<div id="search-container" class="col-md12">
       <h1>Encontre seu perfil com a LifeTec os melhores para sua empresa</h1>
       <form action="/" method="GET">
           <input type="text" id="search" name="search" class="form-control" placeholder="Procurar um perfil...">
       </form>
   </div> 

   <div id="events-container" class="col-md12">
       @if($search)
       <h1>Buscando por:{{$search}}</h1>
       @else
       <h2>Outros perfis</h2>
       <p class="subtitle">Veja todos nossos perfis</p>
       @endif
       
       <div id="cards-container" class="row">
           @foreach($events as $event)
           <div class="card col-md3">
               <img src="/img/events/{{ $event ->image }}" alt="{{ $event->title }}">
               <div class="card-body">
                   <p class="class">{{ date('d/m/y', strtotime($event->date)) }}</p>
                   <h5 class="card-title">{{ $event->title }}</h5>
                   <p class="card-contratos">X Contratos</p>
                   <a href="/events/{{ $event->id }}" class="btn btn-primary">Saber mais</a>
               </div>
           </div>
           @endforeach
           @if(count($events) == 0 && $search)
            <p>Não foi possivel procurar o perfil com o nome {{$search}}! <a href="/">Ver todos os disponiveis</a></p>
           @elseif(count($events) == 0)
           <p>Não há perfis disponiveis</p>
           @endif
       </div>
   </div>

@endsection