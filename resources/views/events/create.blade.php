@extends('Layouts.Main')

@section('title', 'LifeTec')

@section('content')

<div id="event-create-container" class="col-md-6 offset-md-3">
    <h1>Crie o seu Perfil</h1>
    <form action="/events" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="image">Foto de Perfil</label>
            <input type="file" id="image" name="image" class="form-control-file">
        </div>
        <div class="form-group">
            <label for="title">Nome Completo</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Nome Completo">
        </div>
        <div class="form-group">
            <label for="job">Profissão</label>
            <input type="text" class="form-control" id="job" name="job" placeholder="Profissão"> 
        </div>
        <div class="form-group">
            <label for="age">Idade</label>
            <input type="age" class="form-control" id="age" name="age" placeholder="Idade">
        </div>
        <div class="form-group">
            <label for="date">Data de Nascimento</label>
            <input type="date" class="form-control" id="date" name="date" >
        </div>
        <div class="form-group">
            <label for="city">Cidade</label>
            <input type="text" class="form-control" id="city" name="city" placeholder="Cidade">
        </div>
        <div class="form-group">
            <label for="state">Estado</label>
            <input type="text" class="form-control" id="state" name="state" placeholder="Estado">
        </div>
        <div class="form-group">
            <label for="country">Pais</label>
            <input type="text" class="form-control" id="country" name="country" placeholder="Pais">
        </div>
        <div class="form-group">
            <label for="title">Descrição</label>
            <textarea name="description" id="description" class="form-control" placeholder="Descrição sobre você"></textarea>
        </div>
        <div class="form-group">
            <label for="title">Seus eventos são privados?</label>
            <select name="private" id="private" class="form-control">
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>
            <div class="form-group">
            <label for="title">Quais são suas Redes Sociais?</label>
            <div class="form-group">
                <input type="checkbox" name="items[]" value="Instagram">Instagram
            </div>
            <div class="form-group">
                <input type="checkbox" name="items[]" value="YouTube">YouTube
            </div>
            <div class="form-group">
                <input type="checkbox" name="items[]" value="FaceBook">FaceBook
            </div>
            <div class="form-group">
                <input type="checkbox" name="items[]" value="Twitter">Twitter
            </div>
            <div class="form-group">
                <input type="checkbox" name="items[]" value="Linkedin">Linkedin
            </div>
        </div>
        </div>
        <input type="submit" class="btn btn-primary" value="Criar Perfil">
    </form>
</div>


@endsection