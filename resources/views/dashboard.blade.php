@extends('Layouts.Main')

@section('title', 'LifeTec')

@section('content')

<div class="col-md-10 offset-md-1 dashboard-title-container">
    <h1>Meus Perfis</h1>
</div>
<div class="col-md-10 offset-md-1 dashboard-events-container">
    @if(count($events) > 0)
    @else
    <p>Você não possui Perfil<a href="/events/create">Criar um Perfil</a></p>
    @endif
</div>
@endsection